//============================================================================
//                        -- MEMBRI GRUPPO PROGETTO --
//============================================================================
//
//    NOME/COG:  Luca Samuele Vanzo (735924)
//    NOME/COG:  Matteo D'Aniello Copreni (735235)
//    NOME/COG:  Marco Piovani (735380)
//
//=============================================================================
//              -- EPM900, 8051 - ACQUISIZIONE DATI E CONTROLLO --
//=============================================================================
//
//
//       - Specifiche -
//
//       Il sistema da progettare deve permettere di:
//
//       1. Poter ricevere e trasmettere dati mediante seriale.
//          Utilizzare la struttura ad interrupt.
//
//       2. Utilizzando il punto 1 modificare lo stato dei Led
//          (Porta P2) presi singolarmente mediante input utente.
//
//       3. Collegare e configurare un sensore di temperatura.
//          Mediante struttura ad interrupt ricavare il valore
//          della temperatura espressa in gradi celsius.
//
//
//=============================================================================
//  INCLUDO LIBRERIE SISTEMA
//=============================================================================
                                                    //
#include <REG935.H>                                 // Libreria dispositivo
#include <stdio.h>                                  // Libreria standard I/O
#include <string.h>                                 // Libreria stringhe
                                                    //
//=============================================================================
//  DEFINIZIONE PROTOTIPI PROCEDURE
//=============================================================================
                                                    //
void UARTPrintf (char * stringa);                   // Prototipo Stampa UART
void Delay (unsigned int v);                        // Prototipo funzione Delay
                                                    //
//=============================================================================
//  DEFINIZIONE MASCHERA INTERRUPT I2C
//=============================================================================
                                                    //
void i2c_ISR (void) interrupt 6                     // START INT. 6
{                                                   //-------------------------
    int d;                                          // Dichiaro Buffer
                                                    //-------------------------
    switch (I2STAT)                                 // Controllo Stato I2C
    {                                               //-------------------------
        case 0x08:                                  // Se Opcode "START",
        case 0x10:                                  // Se Opcode "RESTART":
            d       = 0;                            // Resetto buffer a ZERO
            I2DAT   = 0x91;                         // Imposto ADDR DEVICE+FLAG
            STO     = 0;                            // Bit STOP settato ALTO
            SI      = 0;                            // Bit Interrupt BASSO
        break;                                      // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
        case 0x40:                                  // Se Opcode "DATI INVIATI"
            STA   = 0;                              // Bit START settato BASSO
            STO   = 0;                              // Bit STOP settato BASSO
            SI    = 0;                              // Bit Interrupt BASSO
            AA    = 1;                              // ACK Bit settato ALTO
                                                    //-------------------------
            UARTPrintf("I2C: Status Read...\r\n");  // Stampo su canale UART
        break;                                      // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
        case 0x50:                                  // Se Opcode "DATI LETTI"
            if (d == 0)                             // Se Buffer uguale ZERO
            {                                       // [Condizione: SI] START
                d   = I2DAT << 8;                   // Sposto per appendere...
                STO = 0;                            // Bit STOP settato BASSO
            }                                       // [Condizione: SI] STOP
            else                                    // Altrimenti...
            {                                       // [Condizione: NO] START
                d  |= I2DAT;                        // Appendo il dato al buff.
                d >>= 3;                            // Tronco 3 BIT
                d  &= 0x1FFF;                       // Pulisco la rotazione
                d  /= 16;                           // Divido per 16 il dato
                                                    //-------------------------
                printf("I2C: Data ->  %d \r\n", d); // Stampo il dato STD::UART
                                                    //-------------------------
                STO = 1;                            // Bit STOP settato ALTO
            }                                       // [Condizione: NO] STOP
                                                    //-------------------------
            STA     = 0;                            // Bit START settato BASSO
            SI      = 0;                            // Bit Interrupt BASSO
            AA      = 1;                            // ACK Bit settato ALTO
        break;                                      // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
        default:                                    // Se Opcode "UNKNOWN"
            STA     = 0;                            // Bit START settato BASSO
            STO     = 0;                            // Bit STOP settato BASSO
            AA      = 1;                            // ACK Bit settato ALTO
        break;                                      // - TERMINE SCOPO OPCODE -
    }                                               //-------------------------
}                                                   // - TERMINE INTERRUPT 6 -
                                                    //
//==============================================================================
//  DEFINIZIONE MASCHERA INTERRUPT UART
//==============================================================================
                                                    //
void uart_ISR (void) interrupt 4                    // START INT. 4
{                                                   //-------------------------
    char c;                                         // Dichiaro Buffer
                                                    //-------------------------
    if (TI) TI = 0;                                 // Se Tx ALTO: Setto BASSO
    if (RI)                                         // Se Rx ALTO allora...
    {                                               // [Condizione: SI] START
        RI = 0;                                     // Imposto Rx Bit BASSO
        c  = SBUF;                                  // Salvo Dato nel Buffer
                                                    //-------------------------
        switch (c)                                  // Controllo Valore Buffer
        {                                           //-------------------------
            case '1':                               // Se Buff. uguale Char '1'
                P2 ^= 0x01;                         // Inverto Bit LED Pin N.1
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '2':                               // Se Buff. uguale Char '2'
                P2 ^= 0x02;                         // Inverto Bit LED Pin N.2
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '3':                               // Se Buff. uguale Char '3'
                P2 ^= 0x04;                         // Inverto Bit LED Pin N.3
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '4':                               // Se Buff. uguale Char '4'
                P2 ^= 0x08;                         // Inverto Bit LED Pin N.4
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '5':                               // Se Buff. uguale Char '5'
                P2 ^= 0x10;                         // Inverto Bit LED Pin N.5
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '6':                               // Se Buff. uguale Char '6'
                P2 ^= 0x20;                         // Inverto Bit LED Pin N.6
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '7':                               // Se Buff. uguale Char '7'
                P2 ^= 0x40;                         // Inverto Bit LED Pin N.7
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '8':                               // Se Buff. uguale Char '8'
                P2 ^= 0x80;                         // Inverto Bit LED Pin N.8
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '9':                               // Se Buff. uguale Char '9'
                STA = 1;                            // START Comunicazione I2C
                STO = 0;                            // Forzo Bit STOP = BASSO
            break;                                  // - TERMINE SCOPO OPCODE -
                                                    //-------------------------
            case '0':                               // Se Buff. uguale Char '0'
                P2 ^= 0xFF;                         // Inverto tutti i Bit LED
            break;                                  // - TERMINE SCOPO OPCODE -
        }                                           //-------------------------
    }                                               // [Condizione: SI] STOP
}                                                   // - TERMINE INTERRUPT 4 -
                                                    //
//=============================================================================
//  MAIN PROCEDURE
//  Program execution starts here after stack initialization.
//=============================================================================
                                                    //
void main()                                         // Entry Point Programma
{                                                   //
    //=========================================================================
    //  IMPOSTAZIONI BASE
    //=========================================================================
                                                    //
    P2M1    = 0;                                    // Quasi-Bidirectional Mod.
    P1M1    = 0;                                    // Quasi-Bidirectional Mod.
    P2      = 0;                                    // Imposto l'output a ZERO
                                                    //
    //=========================================================================
    //  IMPOSTAZIONE UART
    //=========================================================================
                                                    //
    SCON    = 0x50;                                 // 8 Bit Data, 1 Bit STA/O
    BRGR0   = 0xF0;                                 // BAUDRATE9600 Basso
    BRGR1   = 0x02;                                 // BAUDRATE9600 Alto
    BRGCON  = 0x03;                                 // CLK / (BRGR1+BRGR0) + 16
    ES      = 1;                                    // Interrupt UART Attivo
                                                    //
    //=========================================================================
    //  IMPOSTAZIONI CONVERTITORE
    //=========================================================================
                                                    //
    ADMODA  = 0x30;                                 //  AutoScan, Single Conv.
    ADINS   = 0xF0;                                 //  Abilito Canali AD10-13
    ADMODB  = 0x60;                                 //  CLK (12MHz) / 8 (0x60)
                                                    //  CLK / 8 = 1.5 MHz
    ADCON1  = 0x05;                                 //  Abilito e Avvio ADC
                                                    //
    //=========================================================================
    // IMPOSTAZIONI I2C + PIN/SENSORE
    //=========================================================================
    //
    //
    //                                      P1.2             P1.3
    //                                        |                |
    //                                        |                |
    //      VSS :=========[+]=======[+]=======|================|===> (NEG)
    //      VDD :=====[+]==|=========|========|==[+]=====[+]===|===> (POS)
    //                 |   |         |        |   |       |    |
    //                 |   |         |        |   |       |    |
    //                 |   |         |        |  |<|      |    |
    //                 |   |         +-----+  |  |>|     |<|   |
    //                 |   |               |  |  |<|     |>|   |
    //                 |   |               |  |   |      |<|   |
    //                 |   +----------+-+  |  +-+-+       |    |
    //                 |              | |  |    |         |    |
    //                 +------------+ | |  |    | +-------+----+
    //                              | | |   \   | |
    //                           +--|-|-|---|---|-|--+
    //                           |  + + + + + + + +  |
    //                           |  8 7 6 5 4 3 2 1  |
    //                           |   |  +-----+  |   |
    //                           |   +--| SEN |--+   |
    //                           |      +-----+      |
    //                           +-------------------+
    //                                OPEN  DRAIN
    //
    //
    //=========================================================================
                                                    //
    P1M1   |= 0x0C;                                 // P1M1.3/2 Open-Drain Mod.
    P1M2   |= 0x0C;                                 // P1M1.3/2 Open-Drain Mod.
    I2ADR   = 0x00;                                 // Azzero ADDR (force dbg)
    I2ADR   = 0x90;                                 // ADDR Slave per I2C
                                                    //-------------------------
    I2SCLH  = 0xFF;                                 // Generatore CLK(DC) Alto
    I2SCLL  = 0xFF;                                 // Generatore CLK(DC) Basso
    I2CON   = 0x44;                                 // Avvio Interfaccia con AA
                                                    //-------------------------
    IP1    &= ~0x01;                                // Priorità Int. (Cmp)
    IP1H   &= ~0x01;                                // Priorità Int. Alto (Cmp)
                                                    // Cmp = Complemento BIT
                                                    //-------------------------
    EI2C    = 1;                                    // Interrupt I2C Attivi
                                                    //
    //=========================================================================
    // INTERRUPT GLOBALI
    //=========================================================================
                                                    //
    EA      = 1;                                    // Interrupt Globali Attivi
                                                    //
    //=========================================================================
    // CICLO INFINITO PROGRAMMA
    //=========================================================================
                                                    //
    while (1);                                      // LOOP INFINITO
}                                                   //-------------------------
                                                    //
//=============================================================================
//  UART-PRINT PROCEDURE
//=============================================================================
                                                    //
void UARTPrintf (char * stringa)                    // START FUNZ. CUSTOM UART
{                                                   //-------------------------
    if (!TI) TI = 1;                                // Se Tx basso setto ALTO
    while ( *stringa != '\0' )                      // Ciclo finchè != Tappo
    {                                               //-------------------------
        while (!TI);                                // Attendo che si liberi Tx
        TI   = 0;                                   // Libero Tx -> Setto BASSO
        SBUF = *(stringa++);                        // Sposto in Buffer il Char
    }                                               //-------------------------
}                                                   //  - TERMINE FUNZIONE! -
                                                    //
//=============================================================================
//  DELAY PROCEDURE
//=============================================================================
                                                    //
void Delay (unsigned int delayTime)                 // START FUNZIONE DELAY
{                                                   //-------------------------
    while (--delayTime > 0);                        // Ciclo finchè > ZERO
}                                                   //  - TERMINE FUNZIONE! -
                                                    //
//=============================================================================
//  END PROGRAM
//=============================================================================
